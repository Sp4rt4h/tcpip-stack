#include "machine.h"

//Constructor for a host
Machine::Machine(string inName, int numIntf)
{
	//Set the name and number of interfaces
	mName = inName;
	numIntf = numIntf;

	uint8_t macHolder[6];
	
	//Give each interface a custom mac address
	for (int i = 0; i < numIntf; i++) {

		//Allocate and set up interface
		struct interface *tempIntf = (struct interface *)malloc(sizeof(struct interface));
		setupInterface(tempIntf);

		//Generate a mac address
		for(int j = 0; j < 6; j++)
			macHolder[j] = rand() % 254 + 1;

		//Add the mac address to the interface
		setMACAddr(tempIntf, macHolder);
		
		//Set the interface to not configured
		tempIntf->isConfigured = 0;
		
		//Push the interface to the machine
		interfaces.push_back(tempIntf);

	}
}

//Destructor for a host
Machine::~Machine()
{

	//Free each interface of the machine
	for (vector<struct interface *>::iterator i = interfaces.begin(); i != interfaces.end(); i++)
		free(*i);

	interfaces.clear();
}

//Starts a thread for processing packets
void Machine::startProcessing(void)
{

	keepProcessing = 1;

	this->mThread = thread(processPackets, this);


}

//Main threading function for processing packets
void Machine::processPackets(Machine *m)
{

	//Continuously loop and process packets in each interface
	while (true) {
	
		//Loop through each interface and process the packets
		for (auto i = m->interfaces.begin(); i != m->interfaces.end(); i++)
			processInterfacePackets(*i);
	
	}

}

//Dump each interface and its connection
void Machine::dumpInterfaces(void)
{

	cout << endl << "MACHINE: " << mName << endl;

	int count = 1;

	//Iterate through the interfaces vector and dump each interface
	for (vector<struct interface *>::iterator i = interfaces.begin(); i != interfaces.end(); i++, count++) {
		cout << count << ": ";
		dumpInterface(*i);
	}

}

//Method to send a packet
void Machine::sendPacket(int intfNum, struct packet *p)
{

	struct interface *intf = interfaces[intfNum];

	queuePacket(intf, p);

}



