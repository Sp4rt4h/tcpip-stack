#include "network.h"

Network::Network(string inName)
{
	nName = inName;
}

Network::~Network()
{
	machines.clear();
}

void Network::dumpNetwork(void)
{

	//List the interfaces for each machine
	for (auto i = machines.begin(); i != machines.end(); i++)
		(*i)->dumpInterfaces();

}
