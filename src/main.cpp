#include "main.h"
#include "ethernet.h"


Network mainNet("main");

int main(int argc, char **argv, char **env)
{

	//Make sure the user gave a network file
	if (argc < 2) {
		cout << "You need to supply a config file for configuring a network" << endl << "Use exampleConfig for an example network configuration" << endl;
		cout << "Example command: ./main exampleConfig.txt" << endl << endl;
		return -1;
	}

	//Set up network configuration file
	ifstream configFile(argv[1]);
	if (!configFile.is_open()) {
		cout << "Failed to open network configuration file: " << argv[1] << endl;
		return -1;
	}

	//Set the randomness seed
	srand(time(NULL));

	//Create a network from a configuration file	
	createNetworkFromConfig(configFile);

	//Start the network and allow the user to configure
	cmd_loop();

	return 0;
}

void cmd_loop(void)
{

	char cmd = -1;
	
	//Loop continuously
	while (1 == 1) {
	
		cout << endl;
		cout << "What would you like to do? Type h for a list of options: ";
		cout.flush();
	
		cin >> cmd;
		
		switch (cmd) {
		
		case 49:
			mainNet.dumpNetwork();	
			break;
			
		case 50:
			connectMachines();
			break;
			
		case 51:
			startNetwork();
			break;
		
		case 52:
			sendPacketThroughMachine();
			break;
		
		case 'h':
			cout << "'1' - dump network" << endl;
			cout << "'2' - connect machines" << endl;
			cout << "'3' - start the network" << endl;
			cout << "'4' - send packet through machine" << endl;
			cout << "'h' - show help" << endl;
			cout << "'q' - quit" << endl;
			break;
			
		case 'q':
			exit(0);
			break;
		}
		
		usleep(50);
		
		cmd = -1;
	}

};

int listMachines(void)
{

	int count = 1;

	cout << endl;

	//List the machines
	for (auto i = mainNet.machines.begin(); i != mainNet.machines.end(); i++, count++)
		cout << count << ": " << (*i)->mName << endl;
		
	return count;

}

void connectMachines(void)
{

	//Used for connecting machines
	int m1, m2;
	
	int count = listMachines();

	cout << endl << "Which machines would you like to connect: 'x y'" << endl;
	
	//Read the machine counts so we can list the interfaces
	cin >> m1 >> m2;
	if (m1 > count || m2 > count)
		return;
	
	//List the interfaces of each machine that are disconnected
	Machine *mach1 = mainNet.machines.at(m1-1);
	Machine *mach2 = mainNet.machines.at(m2-1);
	mach1->dumpInterfaces();
	mach2->dumpInterfaces();
	
	cout << endl << "Which interfaces from each machine would you like to connect: 'x y'" << endl;
	cin >> m1 >> m2;

	//Just assume the interface numbers are good
	connectInterfaces(mach1->interfaces.at(m1-1), mach2->interfaces.at(m2-1));
}

void startNetwork(void)
{

	//Set up the thread for each machine and start processing the packets
	for (auto i = mainNet.machines.begin(); i != mainNet.machines.end(); i++) {
		(*i)->startProcessing();
	}

}

void sendPacketThroughMachine(void)
{

	Machine *sendMachine;
	int mach = -1;
	int intf = -1;
	
	listMachines();
	
	cout << "Which machine would you like to send a packet through?" << endl;
	cin >> mach;
	mach--;
	
	sendMachine = mainNet.machines[mach];
	
	sendMachine->dumpInterfaces();
	
	cout << "Which interface do you want to send a packet through?" << endl;
	cin >> intf;
	intf--;
	
	struct packet *newP = allocatePacket();
	strncpy(newP->buffer, "Hello packet\n\0", 14);
	
	constructEthernetPacket(sendMachine->interfaces[intf]->connectedInterface->macAddr, sendMachine->interfaces[intf]->macAddr, 0x68, newP);
	
	sendMachine->sendPacket(intf, newP);

}

void createNetworkFromConfig(ifstream &file)
{
	//Used for holding the line being read in from the file
	string line;

	//First get the network name
	getline(file, line);

	//Get the number of machines
	getline(file, line);
	int numMachines = stoi(line);

	//Clear the line separating from first machine
	getline(file, line);

	//Used for the name placeholder of the machine
	string name;

	//Used for the number of interfaces of the machine
	int numIntf;

	//Read in and set up each machine
	for(int i = 0; i < numMachines; i++) {
	
		//Read in the name
		getline(file, name);

		//Read in the number of interfaces
		getline(file, line);
		numIntf = stoi(line);

		//Go to next machine
		getline(file, line);
		
		//Set up the machine and push it to the Network
		Machine *m1 = new Machine(name, numIntf);
		//m1.dumpInterfaces();
		
		mainNet.machines.push_back(m1);
	}

}

