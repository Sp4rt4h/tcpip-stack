#include "ethernet.h"

//Apply the ethernet layer to the given packet
void constructEthernetPacket(uint8_t *destMAC, uint8_t *srcMAC, uint16_t type, struct packet *p)
{

	//To apply the ethernet frame to a packet, we just move the data in the packet
	//and paste destination mac addr, source mac address, and the type

	//Used for placing the data into the packet
	char *c = p->buffer;

	//Move the data
	memmove(&(p->buffer[strlen(p->buffer)]), p->buffer, 14);

	//Copy the destination mac
	strncpy(c, (char *)destMAC, 6);
	c+=6;
	
	//Copy the source mac
	strncpy(c, (char *)srcMAC, 6);
	c+=6;

	//Copy the type
	strncpy(c, (char *)&type, 2);
}

//Used to tear down the packet and process it
void unwrapEthernetPacket(struct packet *p)
{

	uint8_t destMAC[6];
	uint8_t srcMAC[6];
	uint16_t type;
	
	strncpy((char *)destMAC, p->buffer, 6);
	
	strncpy((char *)srcMAC, &(p->buffer[6]), 6);
	
	strncpy ((char *)&type, &(p->buffer[12]), 2);
	
	printf("\nPacket received:\n");
	printf("Destination MAC:\n");
	for(int i = 0; i < 6; i++) {
		printf("%02X", destMAC[i]);
		if (i < 5)
			printf(":");
	}
	printf("\n");
	printf("Source MAC:\n");
	for(int i = 0; i < 6; i++) {
		printf("%02X", srcMAC[i]);
		if (i < 5)
			printf(":");
	}
	printf("\n");
	printf("Protocol: %x\n", type);

}
