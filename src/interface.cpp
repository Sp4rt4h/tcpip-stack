
#include "interface.h"
#include "ethernet.h"

//Setup function for interface
int setupInterface(struct interface *intf)
{

	if (!intf)
		return -1;
		
	bzero(intf, sizeof(struct interface));

	if (pthread_mutex_init(&(intf->pLock), NULL) != 0)
		return -1;
		
	intf->isConnected = 0;

	return 0;

}

//Set the mac address of the interface
void setMACAddr(struct interface *intf, uint8_t *addr)
{
	for(int i = 0; i < 6; i++)
		(*intf).macAddr[i] = addr[i];
}

//Set the ip address of the interface
void setIPAddr(struct interface *intf, uint8_t *addr)
{
	for(int i = 0; i < 4; i++)
		(*intf).ipAddr[i] = addr[i];
}

//Connect both interfaces to each other using sockets
void connectInterfaces(struct interface *intf1, struct interface *intf2)
{
	if (intf1->isConnected || intf2->isConnected)
		return;

	intf1->connectedInterface = intf2;
	intf2->connectedInterface = intf1;
	
	intf1->isConnected = 1;
	intf2->isConnected = 1;
}

//Dump the interface's information
void dumpInterface(struct interface *intf)
{

	//Dump mac address
	printf("MAC ADDR: ");
	for (int i = 0; i < 6; i++) {
		printf("%02X", (*intf).macAddr[i]);
		if (i < 5)
			printf(":");	
	}

	//Dump the interface that the given interface is connected to
	if (intf->isConnected) {
		printf(" <-> ");
		
		for(int i = 0; i < 6; i++) {
		printf("%02X", (*(intf->connectedInterface)).macAddr[i]);
		if (i < 5)
			printf(":");	
		}
	}

	printf("\n");
	
	//Dump ip address if configured
	if (intf->isConfigured) {
		printf("IP ADDR: ");
		for (int i = 0; i < 4; i++) {
			printf("%02X", (*intf).ipAddr[i]);
			if (i < 4)
				printf(":");
		}
		printf("\n");	
	}
}

//Function to process all packets in an interface
void processInterfacePackets(struct interface *intf)
{

	//Lock the interface to make sure we are not racing with other threads
	if (pthread_mutex_lock(&intf->pLock) != 0)
		return;

	//Process the receive and send buffers
	processRXPackets(intf);
	processTXPackets(intf);

	//Let other threads control the current interface				
	pthread_mutex_unlock(&intf->pLock);

}

//Function to process all the packets in the receive queue
void processRXPackets(struct interface *intf)
{

	//Loop through each packet in the receive list
	struct packet *p = NULL;
	while(intf->rpHead != NULL) {
		
		p = receivePacket(intf);

		unwrapEthernetPacket(p);
		
		dequeuePacket(intf);
		

	}

}

//Function to process all the packets in the transmit queue
void processTXPackets(struct interface *intf)
{

	//Loop through each packet in the transmit list and send them
	while (intf->tpHead != NULL)
		sendPacket(intf);

}

//Function to insert a packet into the send list
void queuePacket(struct interface *intf, struct packet *p)
{
	if (!(intf && p))
		return;
		
	pthread_mutex_lock(&intf->pLock);


	if (intf->tpHead == NULL) {

		//Set the packet as the head
		intf->tpHead = p;
		p->next = p;
		p->prev = p;

	}
	
	else {

		//Add the packet to the list
		p->next = intf->tpHead;
		p->prev = intf->tpHead->prev;
		intf->tpHead->prev->next = p;
		intf->tpHead->prev = p;			
	
	}

	pthread_mutex_unlock(&intf->pLock);	

}

//Function to remove a packet from the receive list
void dequeuePacket(struct interface *intf)
{

	struct packet *p;

	p = intf->rpHead;

	if (p->next == p) {

		//Remove the packet from the list	
		deallocatePacket(p);
		intf->rpHead = NULL;

	}

	else {

		//Move the head to the next packet
		intf->rpHead = p->next;
		intf->rpHead->prev = p->prev;
		p->prev->next = intf->rpHead;

		deallocatePacket(p);
		
	}

}

//Function send all packets that are queued
struct packet *receivePacket(struct interface *intf)
{

	struct packet *retPacket = intf->rpHead;

	//Return the packet;
	return retPacket;

}

//Function to send a packet across the simulated network
int sendPacket(struct interface *intf)
{

	//Make sure that the interface is valid and we have a packet to send
	if (!(intf && intf->tpHead))
		return -1;

	//Grab the interface that we have connected to
	struct interface *cIntf = intf->connectedInterface;
	
	//The packet that we will send next
	struct packet *currentPacket = intf->tpHead;

	//Set the head of our transmit list to the next packet
	if (intf->tpHead->next == intf->tpHead)
		intf->tpHead = NULL;
	
	else
		intf->tpHead = currentPacket->next;

	//Make sure the interfaces are connected
	if (!intf->isConnected)
		return 0;

	pthread_mutex_lock(&cIntf->pLock);

	//Determine if we are setting the first packet of the rpHead
	if (cIntf->rpHead != NULL) {

		currentPacket->prev = cIntf->rpHead->prev;
		currentPacket->next = cIntf->rpHead;
		
		cIntf->rpHead->prev->next = currentPacket;
		cIntf->rpHead->prev = currentPacket;

	}

	else {

		cIntf->rpHead = currentPacket;
		currentPacket->next = currentPacket;
		currentPacket->prev = currentPacket;
			
	}

	pthread_mutex_unlock(&cIntf->pLock);

	return 0;
}
