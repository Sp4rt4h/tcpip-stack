#include "packet.h"

//Allocates a packet
struct packet *allocatePacket()
{

	//Allocate the packet
	struct packet *p = (struct packet *)malloc(sizeof(struct packet));
	if (!p)
		return p;

	//Allocate the packet's buffer
	char *b = (char *)malloc(1500);	
	if (!b) {
		free(p);
		p = NULL;
		return p;
	}
		
	p->buffer = b;
	
	return p;
}

//Deallocates a packet
void deallocatePacket(struct packet *p)
{
	//Free the packet's buffer
	free (p->buffer);
	p->buffer = NULL;
	
	//Free the packet
	free(p);
}
