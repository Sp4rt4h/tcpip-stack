#ifndef ARP_H
#define ARP_H

//Represents an entry in the arp
struct arp_entry
{
	uint8_t mac[6];
	uint8_t ip[4];
};

//Represents an arp request
struct arp_request
{
	uint8_t hwType[2];
	uint8_t protType[2];
	uint8_t hwSize;
	uint8_t protSize[2];
	uint8_t opcode[2];
	uint8_t senderMAC[6];
	uint8_t senderIP[4];
	uint8_t receiverMAC[6];
	uint8_t receiverIP[6];
};

#endif
