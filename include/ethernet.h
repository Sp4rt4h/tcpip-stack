#ifndef ETHERNET_H
#define ETHERNET_H

#include "utils.h"
#include "interface.h"

//Represents an ethernet packet
struct ethernetFrame
{

	//Preamble for ethernet packet
	//uint8_t preamble[7];
	
	//Start frame delimeter
	//uint8_t start_frame_del;

	//Mac address for communication
	uint8_t destinationMAC[6];
	uint8_t sourceMAC[6];

	//Unused 802.1 tag for vLANs
	//uint8_t 8021_tag[4];

	//The type of network protocol that we are using
	uint16_t etype;

	//The layer 3 and above packet
	uint8_t payload[1500];

	//uint8_t crc[4];

};

//Apply the ethernet layer to the given packet
void constructEthernetPacket(uint8_t *destMAC, uint8_t *srcMAC, uint16_t type, struct packet *p);

//Used to tear down the packet and process it
void unwrapEthernetPacket(struct packet *p);

#endif
