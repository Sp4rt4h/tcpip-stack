#ifndef NETWORK_H
#define NETWORK_H

#include "machine.h"

class Network
{

public:

	//Network constructor
	Network(string);
	
	//Network destructor
	~Network();
	
	//List of machines in the network
	vector<Machine *> machines; 
	
	//Dump the machines in the network
	void dumpNetwork(void);

private:

	//Name of the network
	string nName;



};


#endif
