#ifndef PACKET_H
#define PACKET_H

#include "utils.h"

//Represents a packet
struct packet
{
	//I am only using a next and prev instead of just next
	//because linux uses it
	struct packet *next;
	struct packet *prev;

	//Holds the packet data
	char *buffer;
};

//Allocates a packet
struct packet *allocatePacket();

//Deallocates a packet
void deallocatePacket(struct packet *);

#endif
