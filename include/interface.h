#ifndef INTERFACE_H
#define INTERFACE_H

#include "utils.h"
#include "netutils.h"
#include "packet.h"

#include <pthread.h>

struct interface
{

	bool isConnected;
	struct interface *connectedInterface;

	//Interface's mac address
	uint8_t macAddr[6];
	
	//Interface's ip address
	uint8_t ipAddr[4];
	
	//Flag for if interface is configured
	bool isConfigured;
	
	//Needed so that interface can receive packet and process
	//without race conditions
	pthread_mutex_t pLock;
	
	//Needed to keep track of the packets in the receive buffer
	struct packet *rpHead;
	
	//Needed to keep track of the packets in the transmit buffer
	struct packet *tpHead;
};

//Setup function for interface
int setupInterface(struct interface *);

//Function for setting mac address
void setMACAddr(struct interface *, uint8_t *);

//Function for setting ip address
void setIPAddr(struct interface *, uint8_t *);

//Function for connecting interfaces
void connectInterfaces(struct interface *, struct interface *);

//Function to dump interface
void dumpInterface(struct interface *);

//Function to process every packet in an interface
void processInterfacePackets(struct interface *);

//Function to process all the packets in the receive queue
void processRXPackets(struct interface *);

//Function to process all the packets in the transmit queue
void processTXPackets(struct interface *);

//Function to insert a packet into the send list
void queuePacket(struct interface *, struct packet *);

//Function to remove a packet from a list
void dequeuePacket(struct interface *);

//Function to receive a packet and process it
struct packet *receivePacket(struct interface *);

//Function to send a packet to the connected interface
int sendPacket(struct interface *);

#endif
