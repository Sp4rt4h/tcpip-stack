#ifndef MACHINE_H
#define MACHINE_H

#include "utils.h"
#include "interface.h"

#include <thread>

//A class designed to simulate a host
class Machine
{

public:

	//Name of the machine
	string mName;

	//The interfaces for the machine
	vector<struct interface *>interfaces;

	//The thread that does the processing of the machine's packets
	thread mThread;

	//Constructor for machine
	Machine(string, int);

	//Destructor for machine
	~Machine();

	//Starts a thread for processing packets
	void startProcessing(void);

	//Method to dump the interfaces of a machine
	void dumpInterfaces(void);

	//Method to send a packet through the interface in the offset
	void sendPacket(int, struct packet *);


private:

	//Number of interfaces of the machine
	int numIntf;

	int keepProcessing;
	
	//Main threading function for processing packets
	static void processPackets(Machine *);

};

#endif
