#ifndef MAIN_H
#define MAIN_H

#include "utils.h"
#include "interface.h"

#include "machine.h"
#include "network.h"


void createNetworkFromConfig(ifstream &);

void cmd_loop(void);

int listMachines(void);

void dumpNetwork(void);

void connectMachines(void);

void startNetwork(void);

void sendPacketThroughMachine(void);

#endif
