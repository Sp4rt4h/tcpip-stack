CC = g++
CFLAGS = -g -Wall -I include

SETUP = setup

BUILD_DIR = build

all: $(SETUP) main

$(SETUP):
	mkdir -p build

packet:
	$(CC) -g -Wall -I include -o build/packet.o -c src/packet.cpp

interface:
	$(CC) $(CFLAGS) -o build/interface.o -c src/interface.cpp

ethernet:
	$(CC) $(CFLAGS) -o build/ethernet.o -c src/ethernet.cpp

machine:
	$(CC) $(CFLAGS) -pthread -o build/machine.o -c src/machine.cpp

network:
	$(CC) $(CFLAGS) -o build/network.o -c src/network.cpp

main: packet interface machine network ethernet
	$(CC) $(CFLAGS) -pthread -o main src/main.cpp src/packet.cpp src/interface.cpp src/machine.cpp src/network.cpp src/ethernet.cpp

clean:
	$(RM) -r $(BUILD_DIR)
